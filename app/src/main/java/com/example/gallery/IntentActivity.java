package com.example.gallery;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

/**
 * Класс, предназначенный для отображения изображения
 * в полном разрешении на экране, а также слайда всех изображений
 * галлереи
 *
 * @author KSO 17ИТ17
 */
public class IntentActivity extends AppCompatActivity implements IConstants {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intent_activity);
        createGalleryPager();
    }

    /**
     * Метод, который формирует адаптер изображений
     * и внедряет его в ViewPager
     */
    private void createGalleryPager() {
        Intent intent = getIntent();
        ViewPager viewPager = findViewById(R.id.viewPager);
        String[] galleryList = intent.getStringArrayExtra(KEY_FOR_GALLERY_LIST);
        viewPager.setAdapter(new ImageAdapter(this, galleryList));
        viewPager.setCurrentItem(intent.getIntExtra(KEY_FOR_INDEX_OF_IMAGE, 0));
    }
}
