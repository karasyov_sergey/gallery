package com.example.gallery;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

/**
 * Реализация класса PagerAdapter
 *
 * @author KSO 17ИТ17
 */
public class ImageAdapter extends PagerAdapter {
    private Context mContext;
    private String[] mGalleryList;

    ImageAdapter(Context context, String[] arrayList) {
        this.mContext = context;
        this.mGalleryList = arrayList;
    }

    @Override
    public int getCount() {
        return mGalleryList.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ImageView) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        RequestOptions requestOptions = new RequestOptions();
        Glide.with(mContext).asBitmap().load(new File(
                mGalleryList[position])).apply(
                        requestOptions.placeholder(R.mipmap.ic_launcher))
                .into(imageView);
        container.addView(imageView, 0);
        return imageView;
    }
}
