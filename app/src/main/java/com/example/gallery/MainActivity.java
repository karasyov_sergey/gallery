package com.example.gallery;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;


public class MainActivity extends AppCompatActivity implements IConstants {
    String[] allFilesPaths;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1000);
        } else {
            showImages();
        }
    }

    /**
     * Метод, отображающий изображения, находящиеся в папке
     * Images на внутренней памяти устройства, в сетку из 3 колонок
     */
    private void showImages() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Images/";
        allFilesPaths = listAllFiles(path);
        RecyclerView recyclerView = findViewById(R.id.gallery);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(layoutManager);
        optimisation(recyclerView);
        GalleryAdapter adapter = new GalleryAdapter(this, allFilesPaths);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Оптимизация RecyclerView
     *
     * @param recyclerView объект, отображающий список элементов
     */
    private void optimisation(RecyclerView recyclerView) {
        recyclerView.setItemViewCacheSize(40);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }


    /**
     * Загружает список графических файлов из папки
     *
     * @param pathName имя папки
     * @return массив, содержащий все пути к графическим файлам
     */
    private String[] listAllFiles(String pathName) {
        File file = new File(pathName);
        File[] files = file.listFiles(new ImageFilter());
        String[] temp = new String[0];
        if (files != null) {
            temp = new String[files.length];
            for (int i = 0; i < files.length; i++) {
                temp[i] = files[i].getAbsolutePath();
            }
        }
        return temp;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1000 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            showImages();
        } else {
            Toast.makeText(this, NOT_HAVE_PERMISSION,
                    Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    /**
     * Реализация класса RecyclerView.Adapter
     *
     * @author KSO 17ИТ17
     */
    class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
        private String[] mGalleryList;
        private Context context;

        GalleryAdapter(Context context, String[] galleryList) {
            this.context = context;
            this.mGalleryList = galleryList;

        }

        @NonNull
        @Override
        public GalleryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                            int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cell, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(GalleryAdapter.ViewHolder viewHolder, final int position) {
            viewHolder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            setImageFromPath(mGalleryList[position], viewHolder.img);
        }


        @Override
        public int getItemCount() {
            return mGalleryList.length;
        }

        private void setImageFromPath(String path, ImageView image) {
            RequestOptions requestOptions = new RequestOptions();
            Glide.with(context).asBitmap().
                    load(new File(path)).apply(requestOptions.placeholder(
                            R.mipmap.ic_launcher)).
                    into(image);
        }

        /**
         * Реализация класса RecyclerView.ViewHolder
         *
         * @author KSO 17ИТ17
         */
        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            ImageView img;

            ViewHolder(View view) {
                super(view);
                img = view.findViewById(R.id.img);
                img.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, IntentActivity.class);
                intent.putExtra(KEY_FOR_GALLERY_LIST, mGalleryList);
                intent.putExtra(KEY_FOR_INDEX_OF_IMAGE, getAdapterPosition());
                startActivity(intent);
            }
        }

    }

}