package com.example.gallery;

import java.io.File;
import java.io.FileFilter;

/**
 * Реализация интерфейса FileFilter.
 * Предназначен для отбора графических файлов определенных типов
 *
 * @author KSO 17ИТ17
 */
public class ImageFilter implements FileFilter, IConstants {

    @Override
    public boolean accept(File pathname) {
        if (pathname.isFile()) {
            String path = pathname.getAbsolutePath();
            String fileExtension = path.substring(path.lastIndexOf('.') + 1);
            String[] imageExtensions = IMAGE_EXTENSIONS.split(" ");
            for (String ext : imageExtensions) {
                if (fileExtension.equalsIgnoreCase(ext)) {
                    return true;
                }
            }
        }
        return false;
    }
}
