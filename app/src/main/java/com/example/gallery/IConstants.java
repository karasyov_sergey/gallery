package com.example.gallery;

/**
 * Интерфейс, содержащий строковые константы
 *
 * @author KSO 17ИТ17
 */
interface IConstants {
    String IMAGE_EXTENSIONS = "jpg jpeg png ico bmp";
    String KEY_FOR_GALLERY_LIST = "mGalleryList";
    String KEY_FOR_INDEX_OF_IMAGE = "index of clicked image";
    String NOT_HAVE_PERMISSION = "Разрешения на чтение нет";
}
